#!/usr/bin/python
# alsa remote
# author: Ondrej Sika, http://ondrejsika.com, dev@ondrejsika.com

import os

from flask import Flask, render_template

from crossdomain import crossdomain

app = Flask(__name__)

@app.route("/")
@app.route("/m/")
def mobile_view():
    return render_template("response.html")

@app.route("/api/")
@crossdomain(origin="*")
def api_home_process():
    return "api"

@app.route("/api/is/")
@crossdomain(origin="*")
def is_process():
    return "true"

@app.route("/api/action/<action>/")
@crossdomain(origin="*")
def action_process(action):
    if action == "volume_up3":
        os.system("amixer -q set Master 3dB+")
    if action == "volume_down3":
        os.system("amixer -q set Master 3dB-")
    if action == "volume_up":
        os.system("amixer -q set Master 1dB+")
    if action == "volume_down":
        os.system("amixer -q set Master 1dB-")
    if action == "mute":
        os.system("amixer -q set Master mute")
    if action == "unmute":
        os.system("amixer -q set Master unmute")
    if action == "toggle":
        os.system("amixer -q set Master toggle")
    return "ok"

if __name__ == "__main__":
    app.run()